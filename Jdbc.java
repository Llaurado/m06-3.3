import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.sql.*;
import java.text.*;
import java.util.*;

public class Jdbc {

    private static ArrayList<Client> llista;
    private static ArrayList<Comanda> comanda;
    private static ArrayList<Object> total;
    private static Session session;
    private static Session session1;
    private static SessionFactory sessionFactory;
    private static SessionFactory sessionFactory1;

    static {
        try {

            sessionFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Client.class).buildSessionFactory();
            sessionFactory1 = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Comanda.class).buildSessionFactory();

            session = sessionFactory.openSession();
            session1 = sessionFactory1.openSession();
        } catch (Exception e) {

        }

    }

    public static int menu() {
        System.out.println("Operacions:\n1.Borrar CLient\n2.Update client\n3.Mostrar client\n4.Alta nou client\n5.Nova comanda\n6.Mostrar comanda\n7.Generacio resum fact\n0.Sortir");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();

    }

    public static void borrarClient()  {

        Scanner sc = new Scanner(System.in);

        session.beginTransaction();
        session1.beginTransaction();

        System.out.println("Escriu el dni del client");
        for (int i = 0; i < llista.size(); i++) {
            System.out.println("Dni:" + llista.get(i).getDni() + " Nom:" + llista.get(i).getName());
        }
        String client = sc.next();
        String borrar ="delete from comandes where dni_client="+client;
        String borrar2 ="delete from client where nif="+client;
        session.createQuery(borrar).executeUpdate();
        session.createQuery(borrar2).executeUpdate();
        session.getTransaction().commit();
        session1.getTransaction().commit();
        session.close();
        session1.close();

        System.out.println("Client borrat juntamens amb les seves comandes");

    }

    public static void updateclient()  {

        Scanner sc = new Scanner(System.in);

        session.beginTransaction();

        System.out.println("Escriu el dni del client");
        for (int i = 0; i < llista.size(); i++) {
            System.out.println("Dni:" + llista.get(i).getDni() + " Nom:" + llista.get(i).getName());
        }
        int dniclient = sc.nextInt();

        for (int i = 0; i < llista.size(); i++) {
            if (llista.get(i).getDni() == dniclient) {
                System.out.println("2.nom");
                String nom = sc.next();
                System.out.println("3.Es vip?(True or false)");
                Boolean vip = sc.nextBoolean();
                session.createQuery("update client set nom="+nom+",premium="+vip+"where nif="+dniclient).executeUpdate();
            }
        }
        session.getTransaction().commit();
        session.close();

    }


    public static void mostrarclient() {
        Scanner sc = new Scanner(System.in);
        System.out.println("text a introduir per cercar nom del client");
        session.beginTransaction();
        String nom = sc.next();
        String hql="select * from client where nif LIKE %"+nom+"$";

        List <Client> result= session.createQuery(hql).getResultList();
session.getTransaction().commit();
        for (Client c:result) {
            System.out.println(c);
        }
session.close();

    }

    public static void altaClient()  {

        String sentenciaSQL = "insert into client (nif,nom,premium)  values (?,?,?)";

        Scanner sc = new Scanner(System.in);
session.beginTransaction();
        int dni;
        String nom;
        boolean vip;
        Comanda com = null;
        System.out.println("Alta Client-----\n1.dni(sense lletra)");
        dni = sc.nextInt();
        System.out.println("2.nom");
        nom = sc.next();
        System.out.println("3.Es vip?(True or false)");
        vip = sc.nextBoolean();
        session.createQuery("insert into client(nif,nom,premium)values("+dni+","+nom+","+vip+")").executeUpdate();
        session.getTransaction().commit();
        session.close();
        for (int i = 0; i < llista.size(); i++) {
            System.out.println(llista.get(i));
        }

        System.out.println("Client inserit correctament.");


    }

    public static void altacomanda()  {

        Scanner sc = new Scanner(System.in);


        int num_com;
        double preu;
        String data;
        session1.beginTransaction();
        System.out.println("Escriu el dni del client");
        for (int i = 0; i < llista.size(); i++) {
            System.out.println("Dni:" + llista.get(i).getDni() + " Nom:" + llista.get(i).getName());
        }
        int client = sc.nextInt();

        System.out.println("Alta Client-----\n1.num_com");
        num_com = sc.nextInt();

        System.out.println("2.preu");
        preu = sc.nextDouble();

        System.out.println("3.data");
        data = sc.next();
        session1.createQuery("insert into client(nif,nom,premium)values("+num_com+","+preu+","+data+","+client+")").executeUpdate();
        session1.getTransaction().commit();
        session1.close();
        System.out.println("Comanda inserit correctament.");

    }

    public static void mostrarcom() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Escriu el dni del client");
        for (int i = 0; i < llista.size(); i++) {
            System.out.println("Dni:" + llista.get(i).getDni() + " Nom:" + llista.get(i).getName());
        }
        int client = sc.nextInt();

        for (int i = 0; i < llista.size(); i++) {
            if (client == (llista.get(i).getC1().getDni())) {
                System.out.println(comanda.get(i));
            }

        }
    }

    public static void generres_fact(int mes, int year, int dni) {
        Calendar cal = Calendar.getInstance();
        int añoactual = cal.get(Calendar.YEAR);
        int mesactual = cal.get(Calendar.MONTH);
        SessionFactory sessionFactory2=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
        Session session2=sessionFactory2.openSession();
        session2.beginTransaction();


        if (mes > 0 && mes < 13 && year <= añoactual && mes <= mesactual) {
            String call = "call procedure proced_emmagatzemat(" + mes + "," + year + "," + dni + ")";
            session2.createQuery(call).executeUpdate();
            session2.beginTransaction().commit();
            System.out.println("dates correctes");
        } else {
            System.out.println("mes o any incorrectes");
        }
session2.close();

    }

    public static void main(String[] args) throws SQLException, ParseException {
        Scanner sc = new Scanner(System.in);
        int opcio = menu();
        while (opcio != 0) {
            switch (opcio) {
                case 1:
                    borrarClient();
                    break;
                case 2:
                    updateclient();

                    break;
                case 3:
                    mostrarclient();

                    break;
                case 4:
                    altaClient();
                    break;
                case 5:
                    altacomanda();
                    break;
                case 6:
                    mostrarcom();
                    break;
                case 7:
                    System.out.println("Introdueix un mes valid:  ");
                    int mes = sc.nextInt();
                    System.out.println("Introdueix un any valid:  ");
                    int any = sc.nextInt();
                    System.out.println("Introdueix un dni:  ");
                    int dni = sc.nextInt();

                    generres_fact(mes, any, dni);
                    break;
            }

            opcio = menu();
        }
    }
}