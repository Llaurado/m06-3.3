
import javax.persistence.*;

@Entity
@Table(name="client")
public class Client {
    private Comanda c1;
    @Id
    @Column(name="nif")
    private int dni;

    @Column(name="nom")
    private String name;

    @Column(name="premium")
    private boolean premium;

    public Client() {
    }

    public Client( int dni, String name, boolean premium) {
        this.dni = dni;
        this.name = name;
        this.premium = premium;

    }



    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    public Comanda getC1() {
        return c1;
    }


    public void setC1(Comanda c1) {
        this.c1 = c1;
    }

    @Override
    public String toString() {
        return "Client{" +
                "dni=" + dni +
                ", name='" + name + '\'' +
                ", premium=" + premium +
                ", c1=" + c1 +
                               '}';
    }
}


