import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="comandes")
public class Comanda {
@Id
@Column(name="num_com")
    private int num_com;
    @Column(name="preu_total")
    private double preu;
    @Column(name="data")
    private String date;
    @Column(name="dni_client")
    private int dni;

    public Comanda(int num_com, double preu, String date, int dni) {
        this.num_com = num_com;
        this.preu = preu;
        this.date = date;
        this.dni = dni;
    }

    public int getNum_com() {
        return num_com;
    }

    public void setNum_com(int num_com) {
        this.num_com = num_com;
    }

    public double getPreu() {
        return preu;
    }

    public void setPreu(double preu) {
        this.preu = preu;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    @Override
    public String toString() {
        return "Comanda{" +
                "num_com=" + num_com +
                ", preu=" + preu +
                ", date='" + date + '\'' +
                ", dni=" + dni +
                '}';
    }
}
